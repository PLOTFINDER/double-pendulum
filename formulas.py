from math import *
from functions import *


def getTheta1(t1, tv1, l1, m1, t2, tv2, l2, m2, g, step, res):

    num = -g * (2 * m1 + m2) * sin(t1)\
          - m2 * g * sin(t1 - 2 * t2)\
          - 2 * sin(t1 - t2) * m2 * \
          (tv2 ** 2 * l2 + tv1 ** 2 * l1 * cos(t1 - t2))
    denum = l1 * (2 * m1 + m2 - m2 * cos(2 * t1 - 2 * t2))

    result = (num / denum)

    print(result)

    return result


def getTheta2(t1, tv1, l1, m1, t2, tv2, l2, m2, g, step, res):
    num = 2 * sin(t1 - t2) * (tv1 ** 2 * l1 * (m1 + m2) + g * (m1 + m2) * cos(t1) + tv2 ** 2 * l2 * m2 * cos(t1 - t2))
    denum = l2 * (2 * m1 + m2 - m2 * cos(2 * t1 - 2 * t2))

    result = (num / denum)

    return result

import pygame
import time
from pygame.locals import *

from formulas import *
from renderer import *

G = 9.81

LENGTH_1 = 1
MASS_1 = 2

LENGTH_2 = 1
MASS_2 = 2

RESISTANCE = 0.9

ZOOM = 50
STEP = 0.025

WINDOW_SIZE = (600, 600)


def simulate():
    pygame.init()

    theta_1 = 0
    theta_2 = 0
    theta_velocity_1 = 0
    theta_velocity_2 = 0
    theta_accel_1 = 0.01
    theta_accel_2 = -0.001

    framerate = 60
    time_per_frame = 1 / framerate
    last_time = time.time()
    frameCount = 0

    mouse_pos = None
    pause = False

    window = pygame.display.set_mode(WINDOW_SIZE)
    pygame.display.set_caption("$name")
    mainClock = pygame.time.Clock()

    simulation = True

    while simulation:
        delta_time = time.time() - last_time
        while delta_time < time_per_frame:
            delta_time = time.time() - last_time

            for event in pygame.event.get():

                if event.type == QUIT:
                    simulation = False

                if event.type == MOUSEMOTION:
                    mouse_pos = event.pos
                    # print(mouse_pos)

                if event.type == KEYDOWN:
                    if event.key == 32:
                        pause = not pause
                    elif event.key == 1073741886:
                        pass

                if event.type == MOUSEBUTTONDOWN:
                    pass

        if not pause:
            pass

        if frameCount % 10 == 0:
            new_theta_accel_1 = getTheta1(theta_1, theta_velocity_1, LENGTH_1, MASS_1, theta_2, theta_velocity_2, LENGTH_2,
                                      MASS_2, G, STEP, RESISTANCE)
            new_theta_accel_2 = getTheta2(theta_1, theta_velocity_1, LENGTH_1, MASS_1, theta_2, theta_velocity_2, LENGTH_2,
                                      MASS_2, G, STEP, RESISTANCE)

            theta_accel_1 += new_theta_accel_1 * STEP
            theta_accel_2 += new_theta_accel_2 * STEP

            theta_velocity_1 += theta_accel_1
            theta_velocity_2 += theta_accel_2

            theta_1 += theta_velocity_1
            theta_2 += theta_velocity_2

            render(window, WINDOW_SIZE, theta_1, LENGTH_1, theta_2, LENGTH_2, ZOOM)

            theta_1 = normRad(theta_1)
            theta_2 = normRad(theta_2)
            print(theta_accel_1)

        pygame.display.update()

        last_time = time.time()
        frameCount += 1


if __name__ == '__main__':
    simulate()

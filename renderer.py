import pygame
from BasicVector import *
from math import pi

def clearWindow(window):
    window.fill((0, 0, 0))


def render(window, window_size, t1, l1, t2, l2, zoom):
    clearWindow(window)

    line_color = pygame.Color(0, 255, 0)
    circle_color = pygame.Color(255, 0, 0)

    angle1 = t1 + 1/2 * pi
    angle2 = t2 + 1/2 * pi

    point_a = Vec2(window_size[0] / 2, window_size[1] / 2)
    point_b = point_a + Vec2.radiansToVec2(angle1) * l1 * zoom
    point_c = point_b + Vec2.radiansToVec2(angle2) * l2 * zoom

    pygame.draw.line(window, line_color, (point_a.x, point_a.y), (point_b.x, point_b.y))
    pygame.draw.line(window, line_color, (point_b.x, point_b.y), (point_c.x, point_c.y))

    pygame.draw.circle(window, circle_color, (point_a.x, point_a.y), 10)
    pygame.draw.circle(window, circle_color, (point_b.x, point_b.y), 10)
    pygame.draw.circle(window, circle_color, (point_c.x, point_c.y), 10)

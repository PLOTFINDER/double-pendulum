from math import pi


def normalizeValue(value, minValue, maxValue):
    return (value - minValue) / (maxValue - minValue)


def lerp(valA, valB, step):
    result = (1 - step) * valB + step * valA
    return result


def normRad(rad):
    return rad % (2 * pi)


if __name__ == '__main__':
    print(lerp(0, 5, 0.025))
